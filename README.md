# Jupyter Notebook to Google Docs

Extension to JupyterLab that provides "one click" solution to move content from Jupyter Notebook to Google Docs.

![GIF of Prototype](img/jupyter2docs.gif)

## Prototype 

1. User opens Google Docs and [Google Apps (container-bound) Script](https://developers.google.com/apps-script) are trigger.
1. [Google Apps (container-bound) Script](https://developers.google.com/apps-script) adds menu.
1. User clicks in the new menu to add anchor.
1. User clicks in the Jupyter Lab extension.
1. Use [Google Drive (REST) API](https://developers.google.com/drive/api/v3/reference?hl=en) to upload image to Google Drive.
1. Use [Google Docs (REST) API](https://developers.google.com/docs/api/reference/rest) to insert image at the anchor location.

## Development

See
[apps-script.js](apps-script.js) for the Google Apps (container-bound) Script
and
[prototype.ipynb](prototype.ipynb) for the code that still need to migrate to a Jupyter extension.